#!/bin/sh
mkdir env 2>/dev/null

if [ "$CI_COMMIT_BRANCH" = "main" ]; then
    VARIABLE_PREFIX="PROD_"
else 
    VARIABLE_PREFIX="DEV_"
fi    
# TODO: add prefix for tests
# get only varibles with this prefix
sed -n -e "/^${VARIABLE_PREFIX}/p" ./env/.env >> ./env/.env.additional.temp
# remove prefix
sed "s/^${VARIABLE_PREFIX}//g" ./env/.env.additional.temp >> ./env/.env-additional
source ./sync_env.sh
