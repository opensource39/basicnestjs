## Deployment script instruction
I used **main** and **develop** as branches name.
1. Integrate SonarCloud into GitLab for group or organization. It's the one time procedure

1.1 Please follow this [instruction](https://docs.sonarcloud.io/getting-started/gitlab/)

1.2 Create unprotected environment variables (should be created in every repository):
- SONAR_TOKEN=%TOKEN%
- SONAR_HOST_URL=https://sonarcloud.io

1.3 Create file **sonar-project.properties** in the project's root directory (should be in every repository)
```
sonar.projectKey=%projectKey%
sonar.organization=%organization%

# This is the name and version displayed in the SonarCloud UI.
#sonar.projectName=Test
#sonar.projectVersion=1.0

# Path is relative to the sonar-project.properties file. Replace "\" by "/" on Windows.
#sonar.sources=.

# Encoding of the source code. Default is default system encoding
#sonar.sourceEncoding=UTF-8
```
2. Create environment variables for Prod, Dev and general (it's required variables). You can add additional variables if you will need.

   **GitLab Repository->Settings->CI/CD->Variables**

2.1 General variables:
- IMAGE_NAME=%DOCKER_IMAGE_NAME%
- PROJECT_NAME=%PROJECT_NAME%
- SCHEMA=public
- SONAR_TOKEN=%TOKEN%
- SONAR_HOST_URL=https://sonarcloud.io

2.2 Dev variables:
- DATABASE_URL_DEV=postgresql://${PGUSER_DEV}:${PGPASSWORD_DEV}@${PGHOST_DEV}:${PGPORT_DEV}/${PGDATABASE_DEV}?schema=${PGSCHEMA}
- PGDATABASE_DEV=%DATABASE_NAME%
- PGHOST_DEV=%DATABASE_HOST%
- PGPASSWORD_DEV=%DATABASE_PASSWORD%
- PGPORT_DEV=5432
- PGUSER_DEV=%DATABASE_USER%
- PORT_DEV=%APPLICATION_PORT%

2.3 Prod variables:
- DATABASE_URL_PROD=postgresql://${PGUSER_DEV}:${PGPASSWORD_DEV}@${PGHOST_DEV}:${PGPORT_DEV}/${PGDATABASE_DEV}?schema=${PGSCHEMA}
- PGDATABASE_PROD=%DATABASE_NAME%
- PGHOST_PROD=%DATABASE_HOST%
- PGPASSWORD_PROD=%DATABASE_PASSWORD%
- PGPORT_PROD=5432
- PGUSER_PROD=%DATABASE_USER%
- PORT_PROD=%APPLICATION_PORT%

3. Install [Docker](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04-ru) and [Docker Compose](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04-ru) in the instances where project will be deployed

4. Create and install Gitlab runners for prod and dev (separate instances) in the instances

**GitLab Repository->Settings->CI/CD->Runners**

- For dev I used tags: backend, dev
- For prod I used tags: backend, main

4.1 Add the gitlab-runner user to the docker group (to have ability run docker commands without sudo)
```
$ sudo usermod -aG docker gitlab-runner
```

Note: if during the first pipeline run you will get the error, then rename **.bash_logout** file in the gitlab_runner's home dir
```
$ cd /home/gitlab-runner/
$ sudo mv .bash_logout .bash_logout_
```

5. Copy deployment scripts into repository:
- docker-compose-dev.yml
- docker-compose-main.yml
- Dockerfile
- .gitlab-ci.yml
- prepare_env.sh
- prepare_private_env.sh
- get_env.sh
- sync_env.sh
- run_tests.sh
- build_image.sh
- deploy.sh

### That's All Folks

## More about docker-compose-*.yml

In this file we're running 2 containers:
- backend app
- database (Postgres)

You could easily add additional containers like Redis if you will need.
In this case all you need to do is add additional environment variables (REDIS_REPLICATION_MODE, REDIS_PASSWORD, REDIS_PORT) and describe new service.
For example:
```
redis:
    container_name: redis
    image: redis:alpine
    restart: always
    environment:
      - REDIS_REPLICATION_MODE=${REDIS_REPLICATION_MODE}
      - REDIS_PASSWORD=${REDIS_PASSWORD}
    ports:
      - ${REDIS_PORT}:6379
    volumes:
      - redis:/data
    command: redis-server --appendonly yes --requirepass ${REDIS_PASSWORD}
```
Update **depends_on** section in api service:
```
depends_on:
    - postgres
    - redis
```
Don't forget to update vVolumes, to be sure that Redis data won't be lost even after reloading instance.

```
volumes:
  postgres:
    driver: local
  redis:
    driver: local
```
