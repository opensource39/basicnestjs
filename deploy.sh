source ./sync_env.sh
echo "Final image name:" $IMAGE_NAME ", Port:" $PORT
bash ./prepare_private_env.sh

if [ "$CI_COMMIT_BRANCH" = "main" ]; then
    COMPOSE_FILE="docker-compose-main.yml"
else 
    COMPOSE_FILE="docker-compose-dev.yml"
fi 
docker-compose -f $COMPOSE_FILE up --detach --build
