#!/bin/sh
mkdir env 2>/dev/null

 #PRIVATE_ENV_FILE="/home/digitalstage/.env.private"
LOCAL_PRIVATE_ENV_FILE="./env/.env.private"
LOCAL_ADDITIONAL_ENV_FILE="./env/.env-additional"
LOCAL_FULL_ENV_FILE="./env/.env.full"

 #cp "${PRIVATE_ENV_FILE}" "${LOCAL_PRIVATE_ENV_FILE}"
cat ./env/.env >> "${LOCAL_FULL_ENV_FILE}"
 #cat "${LOCAL_PRIVATE_ENV_FILE}" >> "${LOCAL_FULL_ENV_FILE}"
cat "${LOCAL_ADDITIONAL_ENV_FILE}" >> "${LOCAL_FULL_ENV_FILE}"