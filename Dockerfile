###################
# BUILD FOR LOCAL DEVELOPMENT
###################

FROM node:18 As dev

# Create app directory
WORKDIR /usr/src/app

# Copy application dependency manifests to the container image.
# A wildcard is used to ensure copying both package.json AND package-lock.json (when available).
# Copying this first prevents re-running npm install on every code change.
COPY ./package*.json ./

# Install app dependencies
RUN npm install && npm cache clean --force

# Bundle app source
COPY . .

# Creates a "dist" folder with the production build
RUN npm run build

###################
# BUILD FOR PRODUCTION
###################

FROM node:18 As prod

ARG NODE_ENV=prod

# Create app directory
WORKDIR /usr/src/app

COPY --from=dev /usr/src/app ./

EXPOSE ${PORT}

# Start the server using the production build
CMD [ "npm", "run", "start" ]
