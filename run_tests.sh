source ./sync_env.sh
echo "Test image name:" $TEST_IMAGE_NAME
docker build . -t $TEST_IMAGE_NAME --target test --build-arg PORT=$PORT
bash ./prepare_private_env.sh
docker run --name $TEST_IMAGE_NAME --env-file ./env/.env.full $TEST_IMAGE_NAME