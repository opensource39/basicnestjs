#!/bin/sh
mkdir env 2>/dev/null
# save all env to file
env >> ./env/.env
# remove docker unsupported data
sed -r 's/\s+//g' ./env/.env > ./env/.env.temp
mv ./env/.env.temp ./env/.env
# prepare additional env
source ./prepare_env.sh
